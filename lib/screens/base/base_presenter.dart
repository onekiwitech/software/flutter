import 'package:evn/screens/base/base_view.dart';

class BasePresenter<V extends BaseView>{
  V? baseview;

  attachView(V view) {
    baseview = view;
  }

  deattachView() {
    baseview = null;
  }

  V? getView(){
    return baseview;
  }

  V? get view{
    return baseview;
  }
}