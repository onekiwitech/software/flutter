import 'package:evn/model/channels_response.dart';
import 'package:evn/model/device_response.dart';
import 'package:evn/model/devices_response.dart';
import 'package:evn/model/message.dart';
import 'package:evn/screens/base/base_view.dart';

abstract class LoginView extends BaseView {
  void createSuccess(DeviceResponse response);
  void listSuccess(DeviceListResponse response);
  void channelSuccess(ChannelListResponse response);
  void deleteSuccess(Message response);
  void showError();
}