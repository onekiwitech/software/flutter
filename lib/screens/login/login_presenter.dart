import 'package:evn/repository/channel_service.dart';
import 'package:evn/repository/device_group_service.dart';
import 'package:evn/repository/device_service.dart';
import 'package:evn/screens/base/base_presenter.dart';
import 'package:evn/screens/login/login_view.dart';

class LoginPresenter extends BasePresenter<LoginView> {

  //AuthService service = AuthService();
  //DeviceGroupService service = DeviceGroupService();
  ChannelService service = ChannelService();
  //CustomerService service = CustomerService();

  LoginPresenter(view) {
    attachView(view);
  }
  
  void update_customer(){
    service.update_customer(1)
      .then((response) => view!.deleteSuccess(response!))
      .catchError((onError) {
        print(onError);
        getView()!.showError();
    });
  }

  void update_status(){
    service.update_status(1)
      .then((response) => view!.deleteSuccess(response!))
      .catchError((onError) {
        print(onError);
        getView()!.showError();
    });
  }

  void history(){
    service.history(1)
      .then((response) => view!.channelSuccess(response!))
      .catchError((onError) {
        print(onError);
        getView()!.showError();
    });
  }

  void delete(){
    service.delete(2)
      .then((response) => view!.deleteSuccess(response!))
      .catchError((onError) {
        print(onError);
        getView()!.showError();
    });
  }

  
}