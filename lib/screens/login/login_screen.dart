import 'package:evn/model/channels_response.dart';
import 'package:evn/model/device_response.dart';
import 'package:evn/model/devices_response.dart';
import 'package:evn/model/message.dart';
import 'package:evn/screens/home/home_screen.dart';
import 'package:evn/screens/login/login_presenter.dart';
import 'package:evn/screens/login/login_view.dart';
import 'package:flutter/material.dart';
import 'package:evn/values/values.dart';
import 'package:evn/widgets/custom_button.dart';
import 'package:evn/widgets/custom_shape_clippers.dart';
import 'package:evn/widgets/custom_text_form_field.dart';
import 'package:evn/widgets/spaces.dart';
import 'package:page_transition/page_transition.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> implements LoginView{
  bool onCheck = false;

  LoginPresenter? presenter;

  LoginScreenState(){
    presenter = LoginPresenter(this);
  }
  
  @override
  void initState() {
    super.initState();
    presenter!.delete();
  }

  @override
  Widget build(BuildContext context) {
    var heightOfScreen = MediaQuery.of(context).size.height;
    var widthOfScreen = MediaQuery.of(context).size.width;
    ThemeData theme = Theme.of(context);

    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Container(
          child: Stack(
            children: <Widget>[
              Positioned(
                child: ClipPath(
                  clipper: ReverseWaveShapeClipper(),
                  child: Container(
                    height: heightOfScreen * 0.5,
                    width: widthOfScreen,
                    decoration: const BoxDecoration(
                      gradient: Gradients.curvesGradient3,
                    ),
                  ),
                ),
              ),
              ListView(
                padding: const EdgeInsets.all(Sizes.PADDING_0),
                shrinkWrap: true,
                children: <Widget>[
                  SizedBox(
                    height: heightOfScreen * 0.5 * 0.90,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: widthOfScreen * 0.15),
                    child: Text(
                      StringConst.LOG_IN,
                      style: theme.textTheme.displaySmall?.copyWith(
                        color: AppColors.deepBrown,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: heightOfScreen * 0.05,
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: Sizes.MARGIN_20),
                    child: _buildForm(),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    ThemeData theme = Theme.of(context);
    var widthOfScreen = MediaQuery.of(context).size.width;
    var heightOfScreen = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        CustomTextFormField(
          hasTitle: true,
          title: StringConst.EMAIL_2,
          titleStyle: theme.textTheme.titleSmall?.copyWith(
            color: AppColors.deepDarkGreen,
            fontSize: Sizes.TEXT_SIZE_14,
          ),
          textInputType: TextInputType.text,
          hintTextStyle: Styles.customTextStyle(
            color: AppColors.greyShade7,
          ),
          enabledBorder: Borders.customUnderlineInputBorder(
            color: AppColors.lighterBlue2,
          ),
          focusedBorder: Borders.customUnderlineInputBorder(
            color: AppColors.lightGreenShade1,
          ),
          textStyle: Styles.customTextStyle(
            color: AppColors.blackShade10,
          ),
          hintText: StringConst.EMAIL_HINT_TEXT,
        ),
        SpaceH16(),
        CustomTextFormField(
          hasTitle: true,
          title: StringConst.PASSWORD,
          titleStyle: theme.textTheme.titleMedium?.copyWith(
            color: AppColors.deepDarkGreen,
            fontSize: Sizes.TEXT_SIZE_14,
          ),
          textInputType: TextInputType.text,
          hintTextStyle: Styles.customTextStyle(
            color: AppColors.greyShade7,
          ),
          enabledBorder: Borders.customUnderlineInputBorder(
            color: AppColors.lighterBlue2,
          ),
          focusedBorder: Borders.customUnderlineInputBorder(
            color: AppColors.lightGreenShade1,
          ),
          textStyle: Styles.customTextStyle(
            color: AppColors.blackShade10,
          ),
          hintText: StringConst.PASSWORD_HINT_TEXT,
          obscured: true,
        ),
        SpaceH20(),
        Container(
          width: widthOfScreen * 0.6,
          child: CustomButton(
            title: StringConst.LOG_IN_4,
            color: AppColors.deepLimeGreen,
            textStyle: theme.textTheme.bodyMedium?.copyWith(
              color: AppColors.white,
              fontWeight: FontWeight.w700,
              fontSize: Sizes.TEXT_SIZE_16,
            ),
            onPressed: () {
              Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.leftToRight,
                  child: const HomeScreen(),
                ),
              );
            },
          ),
        ),
        SizedBox(
          height: heightOfScreen * 0.04,
        ),
        InkWell(
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: StringConst.DONT_HAVE_AN_ACCOUNT,
                  style: theme.textTheme.bodyLarge?.copyWith(
                    color: AppColors.black,
                    fontSize: Sizes.TEXT_SIZE_16,
                  ),
                ),
                TextSpan(
                  text: StringConst.SIGN_UP,
                  style: theme.textTheme.titleSmall?.copyWith(
                    color: AppColors.deepDarkGreen,
                    fontSize: Sizes.TEXT_SIZE_16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
        SpaceH16(),
      ],
    );
  }

  @override
  void createSuccess(DeviceResponse response) {
    print(response.data.name);
    // TODO: implement createSuccess
  }

  @override
  void showError() {
    // TODO: implement showError
  }

  @override
  void deleteSuccess(Message response) {
    print(response.message);
    // TODO: implement deleteSuccess
  }

  @override
  void listSuccess(DeviceListResponse response){
    print(response.data[0].name);
  }

  @override
  void channelSuccess(ChannelListResponse response) {
    print(response.data[0].createdAt);
    // TODO: implement channelSuccess
  }

}