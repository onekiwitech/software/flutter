// GENERAL
const String BASE_URL = "api.github.com";
const String USERS = "users"; //users/{user}
const String REPOS = "repos"; //users/{user}

const String NOWPAY_URL = "api.nowpay.vn:4000";

const String CUSTOMER_CREATE = "customers/create";
const String CUSTOMER_UPDATE = "customers/update";
const String CUSTOMER_DELETE = "customers/delete";
const String CUSTOMER_DETAIL = "customers/detail";
const String CUSTOMER_LIST = "customers/list";

const String DEVICE_GROUP_CREATE = "device_group/create";
const String DEVICE_GROUP_UPDATE = "device_group/update";
const String DEVICE_GROUP_DELETE = "device_group/delete";
const String DEVICE_GROUP_DETAIL = "device_group/detail";
const String DEVICE_GROUP_LIST = "device_group/list";

const String DEVICE_CREATE = "device/create";
const String DEVICE_UPDATE = "device/update";
const String DEVICE_DELETE = "device/delete";
const String DEVICE_DETAIL = "device/detail";
const String DEVICE_LIST = "device/list";
const String DEVICE_CHANNEL = "device/channel";

const String CHANNEL_UPDATE_CUSTOMER = "channel/update/customer";
const String CHANNEL_UPDATE_STATUS = "channel/update/status";
const String CHANNEL_HISTORY = "channel/history";
const String CHANNEL_DELETE = "channel/delete";

const String ADDRESS_AUTHENTICATION_SERVER = "uptocloud.azurewebsites.net";
const String ADDRESS_STORE_SERVER = "uptocloud.azurewebsites.net";
const bool DEBUG_MODE = true;
const bool HTTPS_ENABLED = false;
const String APPLICATION_TMP_DIRECTORY = "./tmp";