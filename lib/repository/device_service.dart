import 'dart:convert';
import 'dart:io';
import 'package:evn/model/channels_response.dart';
import 'package:evn/model/device_response.dart';
import 'package:evn/model/devices_group_response.dart';
import 'package:evn/model/devices_response.dart';
import 'package:evn/model/message.dart';
import 'package:http/http.dart';

import 'constants.dart';
import 'rest_manager.dart';

// https://github.com/St3p99/UpToCloud-Frontend
class DeviceService {
  static final DeviceService _singleton = DeviceService._internal();

  factory DeviceService() {
    return _singleton;
  }

  DeviceService._internal();

  final RestManager _restManager = RestManager();

  Future<DeviceResponse?> create(String user) async {
    Map<String, dynamic> params = {};
    params["device_group_id"] = "1";
    params["name"] = "asd";
    params["imei"] = "ASSSZXD";
    params[""] = "provider";
    params["user_id"] = "1";
    try {
      Response response = await _restManager.makePostRequest(
        NOWPAY_URL, '/$DEVICE_CREATE', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<DeviceResponse?> update(String user) async {
    int id = 1;
    Map<String, dynamic> params = {};
    params["device_group_id"] = "a2";
    params["name"] = 1;
    params["imei"] = "v1.0";
    try {
      Response response = await _restManager.makePutBodyRequest(
        NOWPAY_URL, '/$DEVICE_UPDATE/$id', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<DeviceResponse?> detail(int id) async {
    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$DEVICE_DETAIL/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<ChannelListResponse?> channel(int id) async {
    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$DEVICE_CHANNEL/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return ChannelListResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<DeviceListResponse?> getList() async {

    Map<String, dynamic> params = {};
    params["limit"] = '10';
    params["offset"] = '1';
    params["search"] = '';

    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$DEVICE_LIST', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceListResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<Message?> delete(int id) async {
    try {
      Response response = await _restManager.makeDeleteRequest(
        NOWPAY_URL, '/$DEVICE_DELETE/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return Message.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }
}
