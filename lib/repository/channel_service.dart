import 'dart:convert';
import 'dart:io';
import 'package:evn/model/channels_response.dart';
import 'package:evn/model/device_response.dart';
import 'package:evn/model/devices_group_response.dart';
import 'package:evn/model/devices_response.dart';
import 'package:evn/model/message.dart';
import 'package:http/http.dart';

import 'constants.dart';
import 'rest_manager.dart';

// https://github.com/St3p99/UpToCloud-Frontend
class ChannelService {
  static final ChannelService _singleton = ChannelService._internal();

  factory ChannelService() {
    return _singleton;
  }

  ChannelService._internal();

  final RestManager _restManager = RestManager();

  Future<Message?> update_customer(int id) async {
    Map<String, dynamic> params = {};
    params["customer_id"] = 1;
    try {
      Response response = await _restManager.makePutBodyRequest(
        NOWPAY_URL, '/$CHANNEL_UPDATE_CUSTOMER/$id', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return Message.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<Message?> update_status(int id) async {
    Map<String, dynamic> params = {};
    params["status"] = 1;
    params["provider"] = 'iso';
    params["user_id"] = 1;
    try {
      Response response = await _restManager.makePutBodyRequest(
        NOWPAY_URL, '/$CHANNEL_UPDATE_STATUS/$id', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return Message.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<ChannelListResponse?> history(int id) async {
    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$CHANNEL_HISTORY/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return ChannelListResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<Message?> delete(int id) async {
    try {
      Response response = await _restManager.makeDeleteRequest(
        NOWPAY_URL, '/$CHANNEL_DELETE/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return Message.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }
}
