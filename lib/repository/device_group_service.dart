import 'dart:convert';
import 'dart:io';
import 'package:evn/model/device_group_response.dart';
import 'package:evn/model/devices_group_response.dart';
import 'package:evn/model/message.dart';
import 'package:http/http.dart';

import 'constants.dart';
import 'rest_manager.dart';

// https://github.com/St3p99/UpToCloud-Frontend
class DeviceGroupService {
  static final DeviceGroupService _singleton = DeviceGroupService._internal();

  factory DeviceGroupService() {
    return _singleton;
  }

  DeviceGroupService._internal();

  final RestManager _restManager = RestManager();

  Future<DeviceGroupResponse?> create(String user) async {
    Map<String, dynamic> params = {};
    params["name"] = "a1234";
    params["number_port"] = 11;
    params["version"] = "01234567890";
    try {
      Response response = await _restManager.makePostRequest(
        NOWPAY_URL, '/$DEVICE_GROUP_CREATE', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceGroupResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<DeviceGroupResponse?> update(String user) async {
    int id = 1;
    Map<String, dynamic> params = {};
    params["name"] = "a2";
    params["number_port"] = 1;
    params["version"] = "v1.0";
    try {
      Response response = await _restManager.makePutBodyRequest(
        NOWPAY_URL, '/$DEVICE_GROUP_UPDATE/$id', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceGroupResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<DeviceGroupResponse?> detail(int id) async {
    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$DEVICE_GROUP_DETAIL/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceGroupResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<DeviceGroupListResponse?> getList() async {

    Map<String, dynamic> params = {};
    params["limit"] = '10';
    params["offset"] = '1';
    params["search"] = '';

    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$DEVICE_GROUP_LIST', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return DeviceGroupListResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<Message?> delete(int id) async {
    try {
      Response response = await _restManager.makeDeleteRequest(
        NOWPAY_URL, '/$DEVICE_GROUP_DELETE/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return Message.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }
}
