import 'dart:convert';
import 'dart:io';
import 'package:evn/model/customer_response.dart';
import 'package:evn/model/customers_response.dart';
import 'package:evn/model/message.dart';
import 'package:http/http.dart';

import 'constants.dart';
import 'rest_manager.dart';

// https://github.com/St3p99/UpToCloud-Frontend
class CustomerService {
  static final CustomerService _singleton = CustomerService._internal();

  factory CustomerService() {
    return _singleton;
  }

  CustomerService._internal();

  final RestManager _restManager = RestManager();

  Future<CustomerResponse?> create(String user) async {
    Map<String, dynamic> params = {};
    params["code"] = "a1234";
    params["name"] = "Thanh";
    params["phone"] = "01234567890";
    params["email"] = "van@son.com";
    params["address"] = "abc";
    params["is_activated"] = "true";
    try {
      Response response = await _restManager.makePostRequest(
        NOWPAY_URL, '/$CUSTOMER_CREATE', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return CustomerResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<CustomerResponse?> update(String user) async {
    int id = 1;
    Map<String, dynamic> params = {};
    params["code"] = "123123";
    params["name"] = "as";
    params["phone"] = "0856475676";
    params["email"] = "abc@gmail.com";
    params["address"] = "abcabc";
    try {
      Response response = await _restManager.makePutBodyRequest(
        NOWPAY_URL, '/$CUSTOMER_UPDATE/$id', params);
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return CustomerResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<CustomerResponse?> detail(int id) async {
    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$CUSTOMER_DETAIL/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return CustomerResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<CustomerListResponse?> getList() async {
    try {
      Response response = await _restManager.makeGetRequest(
        NOWPAY_URL, '/$CUSTOMER_LIST');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return CustomerListResponse.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

  Future<Message?> delete(int id) async {
    try {
      Response response = await _restManager.makeDeleteRequest(
        NOWPAY_URL, '/$CUSTOMER_DELETE/$id');
      if (response.statusCode == HttpStatus.notFound) return null;
      if (DEBUG_MODE) {
        print(response.body);
      }
      return Message.fromJson(jsonDecode(response.body));
    } catch (e) {
      print("searchUserByEmail exception: " + e.toString());
      return null;
    }
  }

}
