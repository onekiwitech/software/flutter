import 'package:evn/model/channel.dart';
import 'dart:convert';

import 'package:evn/model/meta.dart';

class ChannelListResponse{
  final bool success;
  final String message;
  final Meta meta;
  final List<Channel> data;

  ChannelListResponse({
    required this.success,
    required this.message,
    required this.data,
    required this.meta,
  });

  factory ChannelListResponse.fromJson(Map<String, dynamic> json) {
    return ChannelListResponse(
      success: json['success']?? false,
      message: json['message']?? '',
      meta: Meta.fromJson(json['meta']),
      data: List<Channel>.from(
          json['data']
          .map((i) => Channel.fromJson(i))
          .toList())
      
    );
  }

  Map<String, dynamic> toJson() => {
    'success': success,
    'message': message,
    'meta': meta.toJson(),
    'data': jsonEncode(data.map((i) => i.toJson()).toList()).toString(),
  };
}

