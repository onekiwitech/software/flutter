import 'package:evn/model/device_group.dart';

class DeviceGroupResponse{
  final bool success;
  final String message;
  final DeviceGroup data;

  DeviceGroupResponse({
    required this.success,
    required this.message,
    required this.data,
  });

  factory DeviceGroupResponse.fromJson(Map<String, dynamic> json) {
    return DeviceGroupResponse(
      success: json['success']?? false,
      message: json['message']?? '',
      data: DeviceGroup.fromJson(json['data']),
      
    );
  }

  Map<String, dynamic> toJson() => {
    'success': success,
    'message': message,
    'data': data.toJson(),
  };
}
