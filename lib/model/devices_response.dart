import 'package:evn/model/device.dart';
import 'package:evn/model/meta.dart';
import 'dart:convert';

class DeviceListResponse{
  final bool success;
  final String message;
  final Meta meta;
  final List<Device> data;

  DeviceListResponse({
    required this.success,
    required this.message,
    required this.meta,
    required this.data,
  });

  factory DeviceListResponse.fromJson(Map<String, dynamic> json) {
    return DeviceListResponse(
      success: json['success']?? false,
      message: json['message']?? '',
      meta: Meta.fromJson(json['meta']),
      data: List<Device>.from(
          json['data']
          .map((i) => Device.fromJson(i))
          .toList())
      
    );
  }

  Map<String, dynamic> toJson() => {
    'success': success,
    'message': message,
    'meta': meta.toJson(),
    'data': jsonEncode(data.map((i) => i.toJson()).toList()).toString(),
  };
}

