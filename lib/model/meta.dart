class Meta {
  final int total;
  final int limit;
  final int page;
  final int pages;

  Meta({
    required this.total,
    required this.limit,
    required this.page,
    required this.pages,
  });

  factory Meta.fromJson(Map<String, dynamic> json) {
    return Meta(
      total: json['total']?? 0,
      limit: json['limit']?? 0,
      page: json['page']?? 0,
      pages: json['pages']?? 0,
    );
  }

  Map<String, dynamic> toJson() => {
    'total': total,
    'limit': limit,
    'page': page,
    'pages': pages,
  };
}