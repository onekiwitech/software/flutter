import 'package:evn/model/device_group.dart';
import 'package:evn/model/meta.dart';
import 'dart:convert';

class DeviceGroupListResponse{
  final bool success;
  final String message;
  final Meta meta;
  final List<DeviceGroup> data;

  DeviceGroupListResponse({
    required this.success,
    required this.message,
    required this.meta,
    required this.data,
  });

  factory DeviceGroupListResponse.fromJson(Map<String, dynamic> json) {
    return DeviceGroupListResponse(
      success: json['success']?? false,
      message: json['message']?? '',
      meta: Meta.fromJson(json['meta']),
      data: List<DeviceGroup>.from(
          json['data']
          .map((i) => DeviceGroup.fromJson(i))
          .toList())
      
    );
  }

  Map<String, dynamic> toJson() => {
    'success': success,
    'message': message,
    'meta': meta.toJson(),
    'data': jsonEncode(data.map((i) => i.toJson()).toList()).toString(),
  };
}
