class Device {
  final int id;
  final String name;
  final String imei;
  final int groupId;
  final String groupName;
  final String createdAt;
  final String updatedAt;
  final String deletedAt;
  final bool isDeleted;

  Device({
    required this.id,
    required this.name,
    required this.imei,
    required this.groupId,
    required this.groupName,
    required this.createdAt,
    required this.updatedAt,
    required this.deletedAt,
    required this.isDeleted,
  });

  factory Device.fromJson(Map<String, dynamic> json) {
    return Device(
      id: json['id']?? 0,
      name: json['name']?? '',
      imei: json['imei']?? '',
      groupId: json['device_group_id']?? 0,
      groupName: json['device_group_name']?? '',
      createdAt: json['created_at']?? '',
      updatedAt: json['updated_at']?? '',
      deletedAt: json['deleted_at']?? '',
      isDeleted: json['is_deleted']?? false,
      
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'imei': imei,
    'device_group_id': groupId,
    'device_group_name': groupName,
    'created_at': createdAt,
    'updated_at': updatedAt,
    'deleted_at': deletedAt,
    'is_deleted': isDeleted,
  };
}
