class Customer {
  final int id;
  final String name;
  final String phone;
  final String email;
  final String code;
  final String deletedAt;
  final bool isActivated;
  final bool isDeleted;
  final bool isDevice;

  Customer({
    required this.id,
    required this.name,
    required this.phone,
    required this.email,
    required this.code,
    required this.deletedAt,
    required this.isActivated,
    required this.isDeleted,
    required this.isDevice,
  });

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      id: json['id']?? 0,
      name: json['name']?? '',
      phone: json['phone']?? '',
      email: json['email']?? '',
      code: json['code']?? '',
      deletedAt: json['deleted_at']?? '',
      isActivated: json['is_activated']?? false,
      isDeleted: json['is_deleted']?? false,
      isDevice: json['is_device']?? false,
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'phone': phone,
    'email': email,
    'code': code,
    'deleted_at': deletedAt,
    'is_activated': isActivated,
    'is_deleted': isDeleted,
    'is_device': isDevice,
  };
}
