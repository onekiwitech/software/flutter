import 'package:evn/model/device.dart';

class DeviceResponse{
  final bool success;
  final String message;
  final Device data;

  DeviceResponse({
    required this.success,
    required this.message,
    required this.data,
  });

  factory DeviceResponse.fromJson(Map<String, dynamic> json) {
    return DeviceResponse(
      success: json['success']?? false,
      message: json['message']?? '',
      data: Device.fromJson(json['data']),
      
    );
  }

  Map<String, dynamic> toJson() => {
    'success': success,
    'message': message,
    'data': data.toJson(),
  };
}
