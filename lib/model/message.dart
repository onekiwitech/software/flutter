class Message {
  final String message;
  final bool success;

  Message({
    required this.message,
    required this.success,
  });

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      message: json['message']?? '',
      success: json['success']?? false,
    );
  }

  Map<String, dynamic> toJson() => {
    'message': message,
    'success': success,
  };
}