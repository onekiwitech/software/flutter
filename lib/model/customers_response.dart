import 'package:evn/model/customer.dart';
import 'dart:convert';

class CustomerListResponse{
  final bool success;
  final String message;
  final List<Customer> data;

  CustomerListResponse({
    required this.success,
    required this.message,
    required this.data,
  });

  factory CustomerListResponse.fromJson(Map<String, dynamic> json) {
    return CustomerListResponse(
      success: json['success']?? false,
      message: json['message']?? '',
      data: List<Customer>.from(
          json['data']
          .map((i) => Customer.fromJson(i))
          .toList())
    );
  }

  Map<String, dynamic> toJson() => {
    'success': success,
    'message': message,
    'data': jsonEncode(data.map((i) => i.toJson()).toList()).toString(),
  };
}
