class DeviceGroup {
  final int id;
  final int port;
  final String name;
  final String version;
  final String file;
  final String note;
  final String createdAt;
  final String updatedAt;
  final String deletedAt;
  final bool isDeleted;

  DeviceGroup({
    required this.id,
    required this.port,
    required this.name,
    required this.version,
    required this.file,
    required this.note,
    required this.createdAt,
    required this.updatedAt,
    required this.deletedAt,
    required this.isDeleted,
  });

  factory DeviceGroup.fromJson(Map<String, dynamic> json) {
    return DeviceGroup(
      id: json['id']?? 0,
      port: json['number_port']?? 0,
      name: json['name']?? '',
      version: json['version']?? '',
      file: json['file']?? '',
      note: json['note']?? '',
      createdAt: json['created_at']?? '',
      updatedAt: json['updated_at']?? '',
      deletedAt: json['deleted_at']?? '',
      isDeleted: json['is_deleted']?? false,
      
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'number_port': port,
    'name': name,
    'version': version,
    'file': file,
    'note': note,
    'created_at': createdAt,
    'updated_at': updatedAt,
    'deleted_at': deletedAt,
    'is_deleted': isDeleted,
  };
}
