import 'package:evn/model/customer.dart';

class CustomerResponse{
  final bool success;
  final String message;
  final Customer data;

  CustomerResponse({
    required this.success,
    required this.message,
    required this.data,
  });

  factory CustomerResponse.fromJson(Map<String, dynamic> json) {
    return CustomerResponse(
      success: json['success']?? false,
      message: json['message']?? '',
      data: Customer.fromJson(json['data']),
      
    );
  }

  Map<String, dynamic> toJson() => {
    'success': success,
    'message': message,
    'data': data.toJson(),
  };
}
