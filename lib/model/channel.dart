class Channel {
  final int id;
  final int channelId;
  final int customerId;
  final int port;
  final int status;
  final String customerCode;
  final String customerName;
  final String createdAt;
  final String updatedAt;
  final String deletedAt;
  final bool isDeleted;

  final String provider;
  final int userId;
  final String userName;
  

  Channel({
    required this.id,
    required this.channelId,
    required this.customerId,
    required this.port,
    required this.status,
    required this.customerCode,
    required this.customerName,
    required this.createdAt,
    required this.updatedAt,
    required this.deletedAt,
    required this.isDeleted,
    required this.provider,
    required this.userId,
    required this.userName,
  });

  factory Channel.fromJson(Map<String, dynamic> json) {
    return Channel(
      id: json['id']?? 0,
      channelId: json['channel_id']?? 0,
      customerId: json['customer_id']?? 0,
      port: json['port']?? 0,
      status: json['status']?? 0,
      customerCode: json['customer_code']?? '',
      customerName: json['customer_name']?? '',
      createdAt: json['created_at']?? '',
      updatedAt: json['updated_at']?? '',
      deletedAt: json['deleted_at']?? '',
      isDeleted: json['is_deleted']?? false,

      provider: json['provider']?? '',
      userId: json['user_id']?? 0,
      userName: json['user_name']?? '',
      
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'channel_id': channelId,
    'customer_id': customerId,
    'port': port,
    'status': status,
    'customer_code': customerCode,
    'customer_name': customerName,
    'created_at': createdAt,
    'updated_at': updatedAt,
    'deleted_at': deletedAt,
    'is_deleted': isDeleted,

    'provider': provider,
    'user_id': userId,
    'user_name': userName,    
  };
}
